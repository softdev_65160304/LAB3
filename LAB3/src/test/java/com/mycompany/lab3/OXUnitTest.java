package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author sirapolaya
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckWin_O_Row1_00_01_02_output_ture(){
        
        char[][] Table = {{'O','O','O'},{'-','-','-'},{'-','-','-'}};
        char Player = 'O';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_O_Row2_10_11_12_output_ture(){
        
        char[][] Table = {{'-','-','-'},{'O','O','O'},{'-','-','-'}};
        char Player = 'O';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_O_Row3_20_21_22_output_ture(){
        
        char[][] Table = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
        char Player = 'O';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_O_Col1_00_10_20_output_ture(){
        
        char[][] Table = {{'O','-','-'},{'O','-','-'},{'O','-','-'}};
        char Player = 'O';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_O_Col2_01_11_21_output_ture(){
        
        char[][] Table = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        char Player = 'O';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_O_Col3_02_12_22_output_ture(){
        
        char[][] Table = {{'-','-','O'},{'-','-','O'},{'-','-','O'}};
        char Player = 'O';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_O_Diagonal1_00_11_22_output_ture(){
        
        char[][] Table = {{'O','-','-'},{'-','O','-'},{'-','-','O'}};
        char Player = 'O';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_O_Diagonal1_02_11_22_output_ture(){
        
        char[][] Table = {{'-','-','O'},{'-','O','-'},{'O','-','-'}};
        char Player = 'O';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
     @Test
    public void testCheckWin_X_Row1_00_01_02_output_ture(){
        
        char[][] Table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char Player = 'X';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_X_Row2_10_11_12_output_ture(){
        
        char[][] Table = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char Player = 'X';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_X_Row3_20_21_22_output_ture(){
        
        char[][] Table = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char Player = 'X';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_X_Col1_00_10_20_output_ture(){
        
        char[][] Table = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
        char Player = 'X';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_X_Col2_01_11_21_output_ture(){
        
        char[][] Table = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
        char Player = 'X';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_X_Col3_02_12_22_output_ture(){
        
        char[][] Table = {{'-','-','X'},{'-','-','X'},{'-','-','X'}};
        char Player = 'X';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void tesCheckWin_X_Diagonal1_00_11_22_output_ture(){
        
        char[][] Table = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        char Player = 'X';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckWin_X_Diagonal1_02_11_22_output_ture(){
        
        char[][] Table = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        char Player = 'X';
        boolean result = LAB3.isWin(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckDraw_0_output_ture(){
        
        char[][] Table = {{'X','O','X'},{'O','X','O'},{'X','O','O'}};
        char Player = '0';
        boolean result = LAB3.isDraw(Table,Player);
        assertEquals(true, result );
    
    }
    
    @Test
    public void testCheckDraw_X_output_ture(){
        
        char[][] Table = {{'X','O','X'},{'O','X','O'},{'X','O','O'}};
        char Player = 'X';
        boolean result = LAB3.isDraw(Table,Player);
        assertEquals(true, result );
    
    }
     
}

