/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

/**
 *
 * @author sirapolaya
 */
public class LAB3 {

    public static boolean isWin(char[][] Table, char Player) {
        for(int Row=0; Row<3; Row++){
             if(Table[Row][0] == Player && Table[Row][1] == Player && Table[Row][2] == Player){
                 return true;
             }
        }
        
        for(int Col=0; Col<3; Col++){
             if(Table[0][Col] == Player && Table[1][Col] == Player && Table[2][Col] == Player){
                 return true;
             }
        }
        
        return (Table[0][0] != '-' && Table[0][0] == Table[1][1] && Table[1][1] == Table[2][2])
                || (Table[0][2] != '-' && Table[0][2] == Table[1][1] && Table[1][1] == Table[2][0]);
        
        //return false;
    }
    
    public static boolean isDraw(char[][] Table, char Player){
        
        if(Table[0][0] != ('-')&& Table[0][1] != ('-')&& Table[0][2] != ('-')
        && Table[1][0] != ('-')&& Table[1][1] != ('-')&& Table[1][2] != ('-')
        && Table[2][0] != ('-')&& Table[2][1] != ('-')&& Table[2][2] != ('-')){
            return true;
        }
        return false;
    }
}
